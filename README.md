# CovidApp Android SDK

Covid Community Alert is a worldwide open-source standard proposed by a global community to provide a service
that allows the monitoring of interactions among devices in a totally anonymous way. We can notify people at risk
by sending them a clear set of instructions.
This will reduce the number of infected patients and allow them to receive their treatments as soon as possible.

Take a look at our open source, anonimous contact tracing application: [CovidApp](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android)


## Getting started

To integrate our contact tracing protocol to your application, follow these steps, or take a look at our [CovidApp Project](https://gitlab.com/coronavirus-outbreak-control/android-sdk-demo).

1. Add Jitpack and the Firebase Crashlytics plugin to your project-level `build.gradle` file:

``` gradle
buildscript {
    dependencies {
        classpath 'com.google.firebase:firebase-crashlytics-gradle:2.0.0-beta03'
    }
}
```

``` gradle
allprojects {
    repositories {
        maven { url "https://jitpack.io" }
    }
}
```
> View full build.gradle file in our [CovidApp project](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android/-/blob/master/CoronavirusHerdImmunity/build.gradle)


2. Add Firebase and the CovidApp SDK to your app-level `build.gradle` file:

``` gradle
dependencies {
    implementation 'com.google.firebase:firebase-crashlytics:17.0.0-beta03'
    implementation 'com.google.firebase:firebase-analytics:17.3.0'
    
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1' 
    implementation 'io.reactivex.rxjava2:rxjava:2.2.9'
    
    // CovidApp SDK
    implementation 'com.gitlab.coronavirus-outbreak-control:sdk-android:v1.2.4'
}
```
> View full app-level build.gradle file in our [CovidApp project](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android/-/blob/master/CoronavirusHerdImmunity/app/build.gradle)


3. Now create your Java class so that it extends the CovidApplicationSDK, and import the CovidApp SDK package.  
For example:

``` java
import org.coronavirus_outbreak_control.android.sdk.CovidApplicationSDK;
// import the enums that the sdk has, if you want to use them
import org.coronavirus_outbreak_control.android.sdk.enums.ApplicationStatus;
import org.coronavirus_outbreak_control.android.sdk.enums.PatientStatus;

public class CovidApplication extends CovidApplicationSDK {

    (...)

    @Override
    public void onPatientStatusUpdated(PatientStatus patientStatus) {
        /** Notifiy always that patient status was updated **/
    }

    @Override
    public void onApplicationStatusUpdated(ApplicationStatus applicationStatus) {
        /** Notifiy always that application status was updated **/
    }

    @Override
    public Notification.Builder permanentNotificationBuilder() {
        /** Return a notification to be used to keep the beacon service running in background **/
        return new Notification.Builder(this);
    }

    @Override
    public void onPermissionStatusChanged(int status) {
        /** Notify when permissions was changed. See PermissionRequest.PermissionsStatus to check each status meaning **/
    }

}

```
> View the full main activity in our [CovidApp project](https://gitlab.com/coronavirus-outbreak-control/coronavirus-outbreak-control-android/-/blob/master/CoronavirusHerdImmunity/app/src/main/java/com/example/coronavirusherdimmunity/CovidApplication.java)


4. The contact tracing will start once **Bluetooth** and **Location** (always allowed) permissions are granted to your application.  
Your app will send a notification to the user if they have interacted with an infected person.


## R8/Proguard 
If you are using R8 or ProGuard add this line in your proguard rule file:
```
-keep class * extends com.google.crypto.tink.shaded.protobuf.GeneratedMessageLite { *; }
```


All done!

