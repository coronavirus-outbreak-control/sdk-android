package com.example.covidapp_sdk;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getName();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        button.setOnClickListener(view -> {
            initBeaconService();
        });

        Button buttonStop = findViewById(R.id.buttonStopService);
        buttonStop.setOnClickListener(view -> {
            stopBeaconService();
        });
    }

    private void initBeaconService() {
        Disposable disposable = DemoApplication.getInstance()
                .initBeaconService().andThen(DemoApplication.getInstance().registerFirebaseToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
                    Log.d(TAG, "init beacon service");
                    Toast.makeText(getBaseContext(), "Service started", Toast.LENGTH_SHORT).show();
                }, Throwable::printStackTrace);
        compositeDisposable.add(disposable);

    }

    private void stopBeaconService() {
        Disposable disposable = DemoApplication.getInstance()
                .stopBeaconService()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {
                    Log.d(TAG, "stop beacon service");
                    Toast.makeText(getBaseContext(), "Service stoped", Toast.LENGTH_SHORT).show();
                }, Throwable::printStackTrace);
        compositeDisposable.add(disposable);

    }

    @Override
    protected void onStop() {
        super.onStop();
        compositeDisposable.clear();
    }
}
