package com.example.covidapp_sdk;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import org.coronavirus_outbreak_control.android.sdk.CovidApplicationSDK;
import org.coronavirus_outbreak_control.android.sdk.enums.ApplicationStatus;
import org.coronavirus_outbreak_control.android.sdk.enums.LoggingMode;
import org.coronavirus_outbreak_control.android.sdk.enums.PatientStatus;

public class DemoApplication extends CovidApplicationSDK {

    private final String TAG = "DemoApplicationLogger";

    private ApplicationStatus lastAppStatus = ApplicationStatus.ACTIVE;
    private PatientStatus lastStatus = PatientStatus.NORMAL;
    private static DemoApplication instance;

    public static DemoApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    @Override
    public void onPatientStatusUpdated(PatientStatus patientStatus) {
        lastStatus = patientStatus;
        Log.d(TAG, "onPatientStatusUpdated " + patientStatus.name());
    }

    @Override
    public void onApplicationStatusUpdated(ApplicationStatus applicationStatus) {
        lastAppStatus = applicationStatus;
        Log.d(TAG, "onApplicationStatusUpdated " + applicationStatus.name());
    }

    @Override
    public Notification.Builder permanentNotificationBuilder() {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setContentTitle(
                String.format(getString(R.string.permanent_notification), lastAppStatus.toString(), lastStatus.toString()) /*, lastCount*/
        );
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("FOREGROUNDBEACON",
                    "Foreground beacon service", NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Foreground beacon service");
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            channel.enableLights(false);
            channel.enableVibration(false);
            channel.setShowBadge(false);
            channel.setSound(null, null);

            NotificationManager notificationManager = (NotificationManager) getSystemService(
                    Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }
        return builder;
    }

    @Override
    public void onPermissionStatusChanged(int status) {
        Log.d(TAG, "Permission status changed " + status);
    }

    @Override
    public LoggingMode loggingMode() {
        return LoggingMode.SDK_INFORMATION;
    }
}
