package org.coronavirus_outbreak_control.android.sdk.exceptions;

import androidx.annotation.NonNull;

public class BluetoothBLENotSupported extends BeaconException {
    @NonNull
    @Override
    public String getMessage() {
        return "Bluetooth BLE is not supported in this device";
    }
}