package org.coronavirus_outbreak_control.android.sdk.utils;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;

import androidx.annotation.IntDef;
import androidx.core.app.ActivityCompat;
import androidx.core.location.LocationManagerCompat;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public class PermissionRequest {

    private Context context;

    public PermissionRequest(Context packageContext) {
        this.context = packageContext;
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({PERMISSION_STATUS_ALL_ALLOWED, PERMISSION_STATUS_BLUETOOTH_NOT_SUPPORTED, PERMISSION_STATUS_BLUETOOTH_IS_NOT_ENABLED,
            PERMISSION_STATUS_LOCATION_IS_NOT_GRANT, PERMISSION_STATUS_LOCATION_BACKGROUND_ACCESS_IS_NOT_GRANT, PERMISSION_STATUS_LOCATION_SERVICE_IS_NOT_ENABLED})
    public @interface PermissionsStatus {
    }

    public static final int PERMISSION_STATUS_ALL_ALLOWED = 0;
    public static final int PERMISSION_STATUS_BLUETOOTH_NOT_SUPPORTED = 1;
    public static final int PERMISSION_STATUS_BLUETOOTH_IS_NOT_ENABLED = 2;
    public static final int PERMISSION_STATUS_LOCATION_IS_NOT_GRANT = 3;
    public static final int PERMISSION_STATUS_LOCATION_BACKGROUND_ACCESS_IS_NOT_GRANT = 4;
    public static final int PERMISSION_STATUS_LOCATION_SERVICE_IS_NOT_ENABLED = 5;


    /**
     * Check if permissions (Bluetooth and Location) are granted
     *
     * @return PermissionStatus int
     */
    @PermissionsStatus
    public int checkPermissions() {

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            return PERMISSION_STATUS_BLUETOOTH_NOT_SUPPORTED;
        } else if (!bluetoothAdapter.isEnabled()) { // if bluetooth is not enabled go to bluetooth activity in order to enable it
            return PERMISSION_STATUS_BLUETOOTH_IS_NOT_ENABLED;
        }//if location is not enabled go to location activity in order to enable it
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return PERMISSION_STATUS_LOCATION_BACKGROUND_ACCESS_IS_NOT_GRANT;
        } else if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return PERMISSION_STATUS_LOCATION_IS_NOT_GRANT;
        } else if (locationManager != null && !LocationManagerCompat.isLocationEnabled(locationManager)) {
            return PERMISSION_STATUS_LOCATION_SERVICE_IS_NOT_ENABLED;
        }

        return PERMISSION_STATUS_ALL_ALLOWED;
    }
}
