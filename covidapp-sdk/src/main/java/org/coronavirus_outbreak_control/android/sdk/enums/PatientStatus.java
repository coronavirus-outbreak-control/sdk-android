package org.coronavirus_outbreak_control.android.sdk.enums;

/**
 * This Enumeration allow to manage Patient Status:
 *   - NORMAL(0),
 *   - INFECTED(1),
 *   - SUSPECT(2),
 *   - HEALED(3),
 *   - LOW_RISK(4),
 *   - MEDIUM_RISK(5),
 *   - HIGH_RISK(6);
 */
public enum PatientStatus {
    NORMAL(0),
    INFECTED(1),
    SUSPECT(2),
    HEALED(3),
    LOW_RISK(4),
    MEDIUM_RISK(5),
    HIGH_RISK(6);

    private int value;

    public static PatientStatus valueOf(int value) {
        for (PatientStatus status : values()) {
            if (status.getInt() == value)
                return status;
        }
        return PatientStatus.NORMAL;
    }

    public int getInt() {
        return value;
    }

    PatientStatus(int value) {
        this.value = value;
    }
}
