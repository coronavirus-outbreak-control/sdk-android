package org.coronavirus_outbreak_control.android.sdk;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.le.AdvertiseSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.RemoteException;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.work.Configuration;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.android.gms.safetynet.SafetyNet;
import com.google.firebase.iid.FirebaseInstanceId;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.BeaconTransmitter;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.coronavirus_outbreak_control.android.sdk.enums.ApplicationStatus;
import org.coronavirus_outbreak_control.android.sdk.enums.Distance;
import org.coronavirus_outbreak_control.android.sdk.enums.LoggingMode;
import org.coronavirus_outbreak_control.android.sdk.enums.PatientStatus;
import org.coronavirus_outbreak_control.android.sdk.exceptions.BeaconException;
import org.coronavirus_outbreak_control.android.sdk.exceptions.BluetoothAndroidVersion;
import org.coronavirus_outbreak_control.android.sdk.exceptions.BluetoothBLENotSupported;
import org.coronavirus_outbreak_control.android.sdk.utils.BeaconDto;
import org.coronavirus_outbreak_control.android.sdk.utils.PermissionRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import bolts.Task;
import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * CovidApplicationSDK is an abstract public class is used to create the Application to manage Storage,
 * Shared Preferences, API, App Status, Beacon, Firebase.
 */
abstract public class CovidApplicationSDK extends Application implements BootstrapNotifier, BeaconConsumer, Configuration.Provider {

    /**
     * This function has to be overridden in order to set lastStatus to patientStatus
     *
     * @param patientStatus
     */
    public abstract void onPatientStatusUpdated(PatientStatus patientStatus);

    /**
     * This function has to be overridden in order to set lastAppStatus to applicationStatus
     *
     * @param applicationStatus
     */
    public abstract void onApplicationStatusUpdated(ApplicationStatus applicationStatus);

    /**
     * This function has to be overridden in order to create a permanent Notification that notifies something to User
     *
     * @return Notification.builder
     */
    public abstract Notification.Builder permanentNotificationBuilder();

    /**
     * This function has to be overridden in order to change permission status
     *
     * @param status
     */
    public abstract void onPermissionStatusChanged(@PermissionRequest.PermissionsStatus int status);

    private static final String BEACON_ID = "451720ea-5e62-11ea-bc55-0242ac130003";
    private static final String BEACON_ID_INTERMITTENT = "b1a75e04-5830-4ff8-956f-9ea8c555f462";
    public static final String STATUS_UPDATE_RECEIVER_ACTION = "STATUS_UPDATE";
    private static final String TAG = "CovidApp";
    private static final int ALARM_MANAGER_REQUEST_CODE = 983723;
    private static final int PERMANENT_NOTIFICATION_ID = 2019042023;

    private ApiManager apiManager;
    private Beacon beaconIntermittent;
    private Beacon beaconSteady;
    private BeaconParser beaconParser;

    private BeaconManager beaconManager;
    private BeaconTransmitter beaconTransmitterIntermittent;
    private BeaconTransmitter beaconTransmitterSteady;
    private PendingIntent nextAlarmPendingIntent;
    private Region beaconRegion;

    private PatientStatus lastStatus = PatientStatus.NORMAL;      // Patient Status (NORMAL, INFECTED, QUARANTINE, HEALED, SUSPECT)
    private ApplicationStatus lastAppStatus = ApplicationStatus.ACTIVE; // App Status (ACTIVE if permissions are granted, INACTIVE if at least a permission is not granted)

    private static CovidApplicationSDK instance;
    private StorageManager storageManager;
    private PreferenceManager preferenceManager;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    /**
     * get instance of CovidApplicationSDK
     *
     * @return instance of CovidApplicationSDK
     */
    public static CovidApplicationSDK getInstance() {
        return instance;
    }

    /**
     * Create instances to manage Application
     * - instance of CovidApplicationSDK
     * - Handler
     * - StorageManager
     * - Preference Manager
     * - Api Manager
     * - Patient Status
     * - App Status
     * - beacon Region
     */
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mHandler = new Handler();
        storageManager = new StorageManager(getBaseContext());
        preferenceManager = new PreferenceManager(getBaseContext());
        apiManager = new ApiManager(getApiUrl(), preferenceManager);
        lastStatus = preferenceManager.getPatientStatus();
        lastAppStatus = preferenceManager.getApplicationStatus();
        beaconRegion = new Region("covidSdkRegionId", null, null, null);
    }

    /**
     * Initialize Beacon Service
     *
     * @return
     */
    public final Completable initBeaconService() {
        setupWorkManager();
        long deviceId = preferenceManager.getDeviceId();
        if (deviceId == -1) {
            return safetyNetVerification()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .flatMap(this::createRequest)
                    .flatMapCompletable(res -> {
                        if (res != -1) {  //case on success (have got "token + device id")
                            preferenceManager.setDeviceId(res);   //save device id in shared preferences
                            try {
                                initBeacon(res);
                                return CompletableObserver::onComplete;
                            } catch (BeaconException exception) {
                                exception.printStackTrace();
                                return Completable.error(exception);
                            }
                        }
                        return Completable.error(new Throwable("Register in server error"));
                    });
        }

        return Completable.defer(() -> {
            initBeacon(deviceId);
            return CompletableObserver::onComplete;
        });
    }

    public String getSafetyNetApiSiteKey() {
        return "6LcxTeUUAAAAAFeCan-0kQdEhz0_B6wtlPFCFfq3";
    }

    private Single<String> safetyNetVerification() {
        return Single.create(emitter -> {
            SafetyNet.getClient(this).verifyWithRecaptcha(getSafetyNetApiSiteKey()).addOnSuccessListener(recaptchaTokenResponse -> {
                emitter.onSuccess(recaptchaTokenResponse.getTokenResult());
            }).addOnFailureListener(throwable -> {
                Logger.e(TAG, "safetynet Request error", throwable);
                emitter.onError(throwable);
            });
        });
    }

    /**
     * Register Firebase Token in order to manage this functionality
     *
     * @return
     */
    public final Completable registerFirebaseToken() {
        return getToken().flatMapCompletable(this::registerOnServer);
    }

    private Completable registerOnServer(Pair<Long, String> longStringPair) {
        return Completable.create(emitter -> {
            try {
                apiManager.registerPushToken(longStringPair.first, longStringPair.second, preferenceManager.getAuthToken());
                emitter.onComplete();
            } catch (Exception e) {
                emitter.onError(e);
            }
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io());
    }

    private Single<Pair<Long, String>> getToken() {
        return Single.create((SingleEmitter<Pair<Long, String>> emitter) -> {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            emitter.onError(task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        if (task.getResult() != null) {
                            Long deviceId = preferenceManager.getDeviceId();
                            Pair<Long, String> pair = new Pair<>(deviceId, task.getResult().getToken());
                            emitter.onSuccess(pair);
                            return;
                        }
                        emitter.onError(new Throwable("Task getResult is null"));
                    })
                    .addOnFailureListener(emitter::onError);
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io());
    }

    /**
     * Register a Device after that recaptcha has been valitated and taken the challenge
     * If the registration has been successful then Authorization Token is received
     *
     * @param challenge the response from Server when reCaptcha has been valitated
     * @return
     */
    private Single<Long> createRequest(String challenge) {
        return Single.create(emitter -> {
            String deviceUUID = new PreferenceManager(getApplicationContext()).getDeviceUUID();
            if (challenge != null) {
                JSONObject object = apiManager.registerDevice(deviceUUID, challenge); //call registerDevice
                if (object != null) {
                    if (object.has("token")) {
                        try {
                            preferenceManager.setAuthToken(object.getString("token"));  //save auth token in shared preferences
                        } catch (JSONException e) {
                            e.printStackTrace();
                            emitter.onError(e);
                        }
                    }
                    emitter.onSuccess(object.getLong("id"));
                }
            }
            emitter.onSuccess(-1L);
        });
    }

    public final boolean isBeaconServiceRunning() {
        return beaconManager != null && beaconManager.isAnyConsumerBound();
    }

    /**
     * Initialize and manage Beacon of a device Id
     *
     * @param deviceId Id of the smartphone/device
     * @throws BeaconException
     */
    public final void initBeacon(long deviceId) throws BeaconException {
        if (isBeaconServiceRunning())
            return;

        beaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(this);

        beaconIntermittent = new Beacon.Builder()
                .setId1(BEACON_ID_INTERMITTENT)
                .setId2("0")
                .setId3("0")
                .setManufacturer(0x004c)
                .setTxPower(-59)
                .setDataFields(new ArrayList<Long>())
                .build();

        beaconSteady = new Beacon.Builder()
                .setId1(BEACON_ID)
                .setId2(String.valueOf(deviceId % 65536)) // major
                .setId3(String.valueOf(deviceId / 65536)) // minor
                .setManufacturer(0x004c)
                .setTxPower(-59)
                .setDataFields(new ArrayList<Long>())
                .build();

        beaconParser = new BeaconParser()
//                .setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25");
                .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24");

        beaconTransmitterSteady = null;
        beaconTransmitterIntermittent = new BeaconTransmitter(getBaseContext(), beaconParser);
        beaconTransmitterIntermittent.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED);
        beaconTransmitterIntermittent.setAdvertiseTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);

        int transmissionSupportedStatus = BeaconTransmitter.checkTransmissionSupported(getBaseContext());
        if (transmissionSupportedStatus == BeaconTransmitter.NOT_SUPPORTED_BLE) {
            throw new BluetoothBLENotSupported();
        } else if (transmissionSupportedStatus == BeaconTransmitter.NOT_SUPPORTED_MIN_SDK) {
            throw new BluetoothAndroidVersion();
        } else if (transmissionSupportedStatus == BeaconTransmitter.SUPPORTED
                || transmissionSupportedStatus == BeaconTransmitter.NOT_SUPPORTED_CANNOT_GET_ADVERTISER
                || transmissionSupportedStatus == BeaconTransmitter.NOT_SUPPORTED_CANNOT_GET_ADVERTISER_MULTIPLE_ADVERTISEMENTS) {

            if (transmissionSupportedStatus == BeaconTransmitter.NOT_SUPPORTED_CANNOT_GET_ADVERTISER
                    || transmissionSupportedStatus == BeaconTransmitter.NOT_SUPPORTED_CANNOT_GET_ADVERTISER_MULTIPLE_ADVERTISEMENTS)
                onPermissionStatusChanged(PermissionRequest.PERMISSION_STATUS_BLUETOOTH_IS_NOT_ENABLED);

            beaconTransmitterIntermittent.startAdvertising(beaconIntermittent);
            mHandler.post(resetTransmission);
        }
        beaconManager.getBeaconParsers().clear();
        beaconManager.getBeaconParsers().add(new BeaconParser().
                setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"));

        LoggingMode logMode = loggingMode();
        BeaconManager.setDebug(logMode == LoggingMode.ALL || logMode == LoggingMode.BEACON_WATCHER);

        beaconManager.enableForegroundServiceScanning(permanentNotificationBuilder().build(), PERMANENT_NOTIFICATION_ID);
        beaconManager.setEnableScheduledScanJobs(false);
        beaconManager.setBackgroundBetweenScanPeriod(500);
        beaconManager.setBackgroundScanPeriod(600);

        // If you wish to test beacon detection in the Android Emulator, you can use code like this:
        // BeaconManager.setBeaconSimulator(new TimedBeaconSimulator() );
        // ((TimedBeaconSimulator) BeaconManager.getBeaconSimulator()).createTimedSimulatedBeacons();

        beaconManager.bind(this);
    }

    private Handler mHandler;

    private Runnable resetTransmission = new Runnable() {
        @Override
        public void run() {
            PermissionRequest permissions = new PermissionRequest(getBaseContext());
            int permissionStatus = permissions.checkPermissions();
            if (permissionStatus == PermissionRequest.PERMISSION_STATUS_ALL_ALLOWED) {  //if bluetooth and location are granted -> enable transmission
                if (lastAppStatus == ApplicationStatus.INACTIVE) {  //1: Inactive. Used to update just one time permanent notification when the permission is granted

                    preferenceManager.setApplicationStatus(ApplicationStatus.ACTIVE);
                    lastAppStatus = preferenceManager.getApplicationStatus();
                    lastStatus = preferenceManager.getPatientStatus();
                    onApplicationStatusUpdated(lastAppStatus);
                    onPatientStatusUpdated(lastStatus);
                    onPermissionStatusChanged(PermissionRequest.PERMISSION_STATUS_ALL_ALLOWED);
                }
                enableTransmission();

            } else {  //if bluetooth or location is not granted -> send a notification in order to alert the User
                if (lastAppStatus == ApplicationStatus.ACTIVE) {  //0: Active and is not first time launch -> Used to send just one notification when the permission are not granted
                    preferenceManager.setApplicationStatus(ApplicationStatus.INACTIVE);
                    lastAppStatus = preferenceManager.getApplicationStatus();
                    lastStatus = preferenceManager.getPatientStatus();
                    onApplicationStatusUpdated(lastAppStatus);
                    onPatientStatusUpdated(lastStatus);
                }
                onPermissionStatusChanged(permissionStatus);
            }

            scheduleNextAlarm();
            mHandler.postDelayed(() -> disableTransmission(), DateUtils.SECOND_IN_MILLIS * 10);
        }
    };

    /**
     * Control what kind of logs will be printed in logcat
     *
     * @return Choose type - Default DebugLogMode.NONE
     * @see LoggingMode
     */
    public LoggingMode loggingMode() {
        return LoggingMode.NONE;
    }

    /**
     * Disable Transmission of Beacons
     */
    private void disableTransmission() {
        Logger.e(TAG, "Transmission stop");
        if (beaconTransmitterIntermittent != null) {
            beaconTransmitterIntermittent.stopAdvertising();
            beaconTransmitterIntermittent = null;
        }
    }

    /**
     * Enable Transmission of Beacons
     */
    private void enableTransmission() {
        Logger.e(TAG, "Transmission start");
        if (beaconTransmitterIntermittent == null && beaconIntermittent != null) {
            beaconTransmitterIntermittent = new BeaconTransmitter(getBaseContext(), beaconParser);
            beaconTransmitterIntermittent.startAdvertising(beaconIntermittent);
        }
        if (beaconTransmitterSteady == null && beaconSteady != null) {
            beaconTransmitterSteady = new BeaconTransmitter(getBaseContext(), beaconParser);
            beaconTransmitterSteady.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED);
            beaconTransmitterSteady.setAdvertiseTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);
            beaconTransmitterSteady.startAdvertising(beaconSteady);
        }
    }

    @Override
    public void didEnterRegion(Region region) {
        Logger.d(TAG, "FXX did enter region.");
    }

    @Override
    public void didExitRegion(Region region) {
        Logger.d(TAG, "I no longer see a beacon.");
    }

    @Override
    public void didDetermineStateForRegion(int state, Region region) {
        Logger.d(TAG, "Current region state is: " + (state == 1 ? "INSIDE" : "OUTSIDE (" + state + ")"));
    }

    /**
     * Service Beacon try to connect
     */
    @Override
    public final void onBeaconServiceConnect() {
        beaconManager.removeAllRangeNotifiers();
        beaconManager.addRangeNotifier(new RangeNotifier() {
            @Override
            public void didRangeBeaconsInRegion(Collection<Beacon> beacons, Region region) {
                Logger.d(TAG, "didRangeBeaconsInRegion called with beacon count:  " + beacons.size());
                final List<BeaconDto> beaconDtos = new ArrayList<>();
                double x = 0;
                double y = 0;
                for (Beacon beacon : beacons) {
                    if (beacon.getId1().toString().equals(BEACON_ID)) {

                        // id2 major - id3 minor
                        long deviceId = 65536 * beacon.getId3().toInt() + beacon.getId2().toInt();

                        Distance distance = Distance.FAR;
                        if (beacon.getDistance() <= 0.4) {
                            distance = Distance.IMMEDIATE;
                        } else if (beacon.getDistance() <= 2) {
                            distance = Distance.NEAR;
                        }

                        double distanceFilter = preferenceManager.getDistanceFilter();
                        if (distanceFilter < 0 || beacon.getDistance() <= distanceFilter) {
                            boolean sendUserLocation = preferenceManager.getUserLocationPermission();

                            if (sendUserLocation && x == 0 && y == 0) {
                                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                                if (locationManager != null &&
                                        (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                                                ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                                    Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                                    x = (location == null ? 0 : location.getLatitude());
                                    y = (location == null ? 0 : location.getLongitude());
                                }
                            }
                            BeaconDto beaconDto = new BeaconDto(deviceId, beacon.getRssi(), distance, beacon.getDistance(), x, y);
                            beaconDtos.add(beaconDto);
                        }
                    }
                }

                Task.callInBackground(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        if (beaconDtos.size() > 0)
                            storageManager.insertBeacon(beaconDtos);

                        if (lastStatus != preferenceManager.getPatientStatus()) {
                            lastStatus = preferenceManager.getPatientStatus();
                            lastAppStatus = preferenceManager.getApplicationStatus();
                            onApplicationStatusUpdated(lastAppStatus);
                            onPatientStatusUpdated(lastStatus);
                        }
                        return null;
                    }
                });
            }
        });

        try {
            beaconManager.startRangingBeaconsInRegion(beaconRegion);
        } catch (RemoteException e) {
            Logger.e(TAG, "Start ranging beacon in region error", e);
        }
    }

    public final void userSendLocationPermissionChanged(boolean permissionGrant) {
        preferenceManager.setUserLocationPermission(permissionGrant);
    }

    public final void updatePermanentNotification(Notification.Builder notificationBuilder) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);

        if (notificationManager != null)
            notificationManager.notify(PERMANENT_NOTIFICATION_ID, notificationBuilder.build());
    }

    /**
     * Stop Beacon Service
     *
     * @return
     */
    public final Completable stopBeaconService() {
        return Completable.create(emitter -> {
            //stop steady beacon
            if (beaconTransmitterSteady != null) {
                beaconTransmitterSteady.stopAdvertising();
                beaconTransmitterSteady = null;
            }

            //stop intermittent beacon
            disableTransmission();

            cancelNextAlarm();

            try {
                beaconManager.stopRangingBeaconsInRegion(beaconRegion);
                beaconManager.unbind(this);
            } catch (Exception e) {
                Logger.e(TAG, "Start ranging beacon in region error", e);
            }

            emitter.onComplete();
        });
    }

    private void cancelNextAlarm() {
        AlarmManager am = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
        if (am == null || nextAlarmPendingIntent == null) return;

        am.cancel(nextAlarmPendingIntent);
    }

    private void scheduleNextAlarm() {
        AlarmManager am = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
        if (am == null) return;

        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        nextAlarmPendingIntent = PendingIntent.getBroadcast(getBaseContext(), ALARM_MANAGER_REQUEST_CODE, intent, 0);

        long nextVerification = getNextVerificationTimestamp();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, nextVerification, nextAlarmPendingIntent);
        } else {
            am.setExact(AlarmManager.RTC_WAKEUP, nextVerification, nextAlarmPendingIntent);
        }
    }

    public void onNewToken(String token) {
        if (!preferenceManager.isFirstTimeLaunch()) {
            long deviceId = preferenceManager.getDeviceId();
            Disposable disposable = registerOnServer(new Pair<>(deviceId, token)).subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
                    .subscribe(() -> {
                        Logger.d(TAG, "Firebase token updated");
                    }, throwable -> {
                        Logger.e(TAG, "Firebase token update error", throwable);
                    });
            compositeDisposable.add(disposable);
        }
    }

    private long getNextVerificationTimestamp() {
        Calendar nextVerification = Calendar.getInstance();
        int currentMinute = nextVerification.get(Calendar.MINUTE);
        int currentHour = nextVerification.get(Calendar.HOUR_OF_DAY);
        int nextHour = currentHour;
        int nextMinute;
        if (currentMinute == 59) {
            if (currentHour == 23) {
                nextHour = 0;
            } else {
                nextHour = currentHour + 1;
            }
            nextMinute = 0;
        } else {
            nextMinute = currentMinute + 1;
        }
        nextVerification.set(Calendar.HOUR_OF_DAY, nextHour);
        nextVerification.set(Calendar.MINUTE, nextMinute);
        nextVerification.set(Calendar.SECOND, 0);
        return nextVerification.getTimeInMillis();
    }

    private void setupWorkManager() {
        setupWorkManager(PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS, false);
    }

    public final void setupWorkManager(long intervalInMilliseconds, boolean shouldReplace) {
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        PeriodicWorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(SyncDatabaseWorkManager.class, intervalInMilliseconds, TimeUnit.MILLISECONDS)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance(getBaseContext())
                .enqueueUniquePeriodicWork(
                        SyncDatabaseWorkManager.TAG,
                        shouldReplace ? ExistingPeriodicWorkPolicy.REPLACE : ExistingPeriodicWorkPolicy.KEEP,
                        saveRequest
                );
    }

    public String getApiUrl() {
        return "https://api.coronaviruscheck.org";
    }

    @NonNull
    @Override
    public final Configuration getWorkManagerConfiguration() {
        return new Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .build();
    }

    public static class AlarmReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CovidApplicationSDK.instance.mHandler.post(CovidApplicationSDK.instance.resetTransmission);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        compositeDisposable.dispose();
    }

    public final long getDeviceID() {
        return preferenceManager.getDeviceId();
    }
}
