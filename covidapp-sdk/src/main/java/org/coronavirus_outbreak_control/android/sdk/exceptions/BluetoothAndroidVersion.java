package org.coronavirus_outbreak_control.android.sdk.exceptions;

import androidx.annotation.NonNull;

import org.coronavirus_outbreak_control.android.sdk.BuildConfig;

public final class BluetoothAndroidVersion extends BeaconException {
    @Override
    @NonNull
    public String getMessage() {
        return "Android " + BuildConfig.VERSION_CODE + " don't support Bluetooth BLE";
    }
}