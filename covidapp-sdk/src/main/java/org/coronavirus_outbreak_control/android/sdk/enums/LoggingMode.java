package org.coronavirus_outbreak_control.android.sdk.enums;

public enum LoggingMode {
    /**
     * Show LoggingMode.BEACON_WATCHER and LoggingMode.SDK_INFORMATION logs in the same time
     * NOT RECOMMENDED
     */
    ALL,

    /**
     * Show all information about beacons: hardware manager, parser and scanning
     * This mode is only recommend to expert users to find bugs. The amount of logs will increase a lot, making it difficult to see important logs
     */
    BEACON_WATCHER,

    /**
     * Information about sdk errors, requests and information logs.
     * This mode is the recommend to all users in debug environment
     */
    SDK_INFORMATION,

    NONE
}
