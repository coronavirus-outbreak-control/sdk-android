package org.coronavirus_outbreak_control.android.sdk.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * This enumeration allows to manage status of the Application:
 *  - Active (0)
 *  - Inactive (1)
 */
public enum ApplicationStatus {

    ACTIVE(0),
    INACTIVE(1);

    private int intValue;

    private static Map<Integer, ApplicationStatus> map = new HashMap<Integer, ApplicationStatus>();

    static {
        for (ApplicationStatus enu : ApplicationStatus.values()) {
            map.put(enu.intValue, enu);
        }
    }

    ApplicationStatus(int value) {
        intValue = value;
    }

    public int toInt() {
        return intValue;
    }

    public static ApplicationStatus valueOf(int value) {
        return map.get(value);
    }

}
