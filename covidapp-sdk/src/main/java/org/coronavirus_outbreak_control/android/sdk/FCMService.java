package org.coronavirus_outbreak_control.android.sdk;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public final class FCMService extends FirebaseMessagingService {

    private static final String TAG = "FCMService";

    public FCMService() {
    }

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        PreferenceManager preferenceManager = new PreferenceManager(getBaseContext());
        ApiManager apiManager = new ApiManager(CovidApplicationSDK.getInstance().getApiUrl(), preferenceManager);

        Disposable disposable = CovidApplicationSDK.getInstance().initBeaconService().subscribe();
        compositeDisposable.add(disposable);
        
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Map<String, String> data = remoteMessage.getData();

            String _status = data.get("status");
            int status = 0;
            if (_status != null) {
                status = Integer.parseInt(_status);
            }
            preferenceManager.setPatientStatus(status);

            String _wlevel = data.get("warning_level");
            int wlevel = 0;
            if (_wlevel != null) {
                wlevel = Integer.parseInt(_wlevel);
            }
            preferenceManager.setWarningLevel(wlevel);

            String _filterId = data.get("filter_id");
            int filterId = 0;
            if (_filterId != null) {
                filterId = Integer.parseInt(_filterId);
            }
            preferenceManager.setAlertFilterId(filterId);

            final String link = data.get("link");
            preferenceManager.setAlertLink(link);

            final String language = data.get("language");
            String fb_language = null;
            try {
                String content = data.get("content");
                if (content != null) {
                    JSONObject jsonObject = new JSONObject(content);
                    fb_language = jsonObject.getString("language");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            preferenceManager.setAlertLanguage(language != null ? language : fb_language);

            if (link != null) {
                final int finalFilterId = filterId;
                Task.callInBackground(new Callable<JSONObject>() {
                    @Override
                    public JSONObject call() throws Exception {
                        return apiManager.downloadAlert(link, finalFilterId, language);
                    }
                }).onSuccess(new Continuation<JSONObject, Object>() {
                    @Override
                    public Object then(Task<JSONObject> task) throws Exception {
                        preferenceManager.setAlertContent(task.getResult());
                        return null;
                    }
                });
            }

            String title = data.get("title");
            String message = data.get("message");

            sendNotification(getBaseContext(), title, message);

            LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(new Intent(CovidApplicationSDK.STATUS_UPDATE_RECEIVER_ACTION));
        }
    }

    @Override
    public void onNewToken(@NonNull String token) {
        CovidApplicationSDK.getInstance().onNewToken(token);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    public static void sendNotification(Context context, String title, String message) {

        Intent intent = new Intent("org.coronavirus_outbreak_control.NOTIFICATION");
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "STATUS_UPDATE";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})       //Vibration
                        .setContentIntent(pendingIntent);
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Status update",
                        NotificationManager.IMPORTANCE_HIGH);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }
    }
}