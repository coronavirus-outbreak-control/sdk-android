package org.coronavirus_outbreak_control.android.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;

import org.coronavirus_outbreak_control.android.sdk.utils.BeaconDto;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

final class ApiManager {

    private final MediaType JSONContentType = MediaType.parse("application/json; charset=utf-8");
    private final String baseEndpoint;
    private final PreferenceManager preferenceManager;
    private final String TAG = getClass().getSimpleName();

    public ApiManager(String endpointUrl, PreferenceManager preferenceManager) {
        baseEndpoint = endpointUrl;
        this.preferenceManager = preferenceManager;
    }

    /**
     * Register Device on Back-end when the User is logged for the first time on App
     *
     * @param deviceId  id of smartphone/device
     * @param challenge response of reCaptcha
     * @return
     */
    public JSONObject registerDevice(String deviceId, String challenge) {

        HashMap<String, Object> device = new HashMap<>();
        device.put("manufacturer", Build.MANUFACTURER);
        device.put("model", Build.MODEL);

        HashMap<String, Object> os = new HashMap<>();
        os.put("name", "Android");
        os.put("version", Build.VERSION.RELEASE);

        HashMap<String, Object> body = new HashMap<>();
        body.put("id", deviceId);
        body.put("os", os);
        body.put("device", device);
        body.put("challenge", challenge);

        RequestBody rq = RequestBody.create(JSONContentType, JSONValue.toJSONString(body));
        Request request = new Request.Builder()
                .url(baseEndpoint + "/device/handshake")
                .post(rq)
                .build();
        try {
            Response response = getOkHttpClient(false).newCall(request).execute();
            String strResponse = response.body().string();
            return new JSONObject(strResponse);
        } catch (Exception e) {
            Logger.e(TAG, "EXCEPTION on registering device", e);
        }
        return null;
    }

    /**
     * Push the interactions between 2 devices to the Back-end
     *
     * @param context
     * @param beacons
     * @param authToken
     * @return
     */
    public JSONObject pushInteractions(Context context, List<BeaconDto> beacons, String authToken) {
        if (beacons == null || beacons.size() == 0) {
            return null;
        }

        JSONArray arr = new JSONArray();
        for (BeaconDto beacon : beacons) {
            if (beacon.getJSON() != null) {
                arr.put(beacon.getJSON());
            }
        }

        JSONObject body = new JSONObject();
        try {
            body.put("i", preferenceManager.getDeviceId());
            body.put("p", "a");

            try {
                PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                int verCode = pInfo.versionCode;
                body.put("v", verCode);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            body.put("z", arr);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody rq = RequestBody.create(JSONContentType, JSONValue.toJSONString(body));
        try {
            Logger.d("BEACONTIME PUSH boby", requestBodyToString(rq));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Request request = new Request.Builder()
                .url(baseEndpoint + "/interaction/report")
                .addHeader("Authorization", "Bearer " + authToken)
                .post(rq)
                .build();
        try {
            Response response = getOkHttpClient(true).newCall(request).execute();
            if (response.code() == 200) {
                String strResponse = response.body().string();
                JSONObject obj = new JSONObject(strResponse);
                return obj;
            } else {
                Logger.d(TAG, "push interactions error " + response.code() + " - " + response.message());
                return null;
            }
        } catch (Exception e) {
            Logger.d(TAG, "EXCEPTION on pushing interaction");
        }
        return null;
    }

    /**
     * Require Body
     *
     * @param requestBody
     * @return
     * @throws IOException
     */
    public String requestBodyToString(RequestBody requestBody) throws IOException {
        Buffer buffer = new Buffer();
        requestBody.writeTo(buffer);
        return buffer.readUtf8();
    }

    /**
     * Register Push Token
     *
     * @param deviceId
     * @param token
     * @param authToken
     * @return
     */
    public final JSONObject registerPushToken(Long deviceId, String token, String authToken) throws Exception {
        HashMap<String, Object> body = new HashMap<>();
        body.put("id", deviceId);
        body.put("push_id", token);
        body.put("platform", "android");

        RequestBody rq = RequestBody.create(JSONContentType, JSONValue.toJSONString(body));
        Request request = new Request.Builder()
                .url(baseEndpoint + "/device")
                .addHeader("Authorization", "Bearer " + authToken)
                .put(rq)
                .build();
        try {
            Response response = getOkHttpClient(true).newCall(request).execute();
            String strResponse = response.body().string();
            if (strResponse.isEmpty())
                return new JSONObject();
            return new JSONObject(strResponse);
        } catch (JSONException e) {
            Logger.e(TAG, "EXCEPTION registering device ", e);
            throw new Exception("Error registering device");
        }
    }

    private class HttpInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            //Build new request
            Request.Builder builder = request.newBuilder();
            builder.header("Accept", "application/json"); //if necessary, say to consume JSON

            String token = preferenceManager.getAuthToken(); //save token of this request for future
            setAuthHeader(builder, token); //write current token to request

            request = builder.build(); //overwrite old request
            Response response = chain.proceed(request); //perform request, here original request will be executed

            if (response.code() == 401) { //if unauthorized
                synchronized (this) { //perform all 401 in sync blocks, to avoid multiply token updates
                    String code = null;
                    try {
                        code = refreshToken();
                        if (code == null) {
                            return response;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            return response;
        }

        private void setAuthHeader(Request.Builder builder, String token) {
            if (token != null) //Add Auth token to each request if authorized
                builder.header("Authorization", String.format("Bearer %s", token));
        }

        /**
         * When Token is expired then is refreshed, saved and returned result code
         */
        private String refreshToken() throws JSONException {

            //you might use retrofit here
            String deviceUUID = preferenceManager.getDeviceUUID();
            String challenge = preferenceManager.getChallenge();
            Logger.d(TAG, "try to refresh token");
            JSONObject object = registerDevice(/*"06c9cf6c-ecfb-4807-afb4-4220d0614593"*/ deviceUUID, challenge);
            if (object != null) {
                if (object.has("token")) {
                    String token = object.getString("token");
                    preferenceManager.setAuthToken(token);
                    Logger.d(TAG, "new token " + token);
                    return token;
                }
            }
            return null;
        }
    }

    /**
     * Download Alert Notification which notifies some contents to User
     *
     * @param url
     * @param filterId
     * @param remoteLanguage
     * @return
     */
    public JSONObject downloadAlert(@NonNull String url, int filterId, String remoteLanguage) {
        String myLocalizedUrl = url.replace("{language}", Locale.getDefault().getLanguage());
        String remoteLocalizedUrl = url.replace("{language}", remoteLanguage);

        OkHttpClient client = getOkHttpClient(true);
        Request request = new Request.Builder()
                .url(myLocalizedUrl)
                .addHeader("Content-Type", "application/json")
                .get()
                .build();

        try {
            Response response = client.newCall(request).execute();
            String strResponse;
            if (response.code() != 200) {
                request = new Request.Builder()
                        .url(remoteLocalizedUrl)
                        .addHeader("Content-Type", "application/json")
                        .get()
                        .build();
                response = client.newCall(request).execute();
            }
            strResponse = response.body().string();

            JSONObject obj = new JSONObject(strResponse);
            JSONArray filters = obj.getJSONArray("filters");
            JSONObject res = null;
            for (int i = 0; i < filters.length(); i++) {
                JSONObject o = filters.getJSONObject(i);
                if (o.has("filter_id") && o.getInt("filter_id") == filterId) {
                    String filterLang = o.getString("language");
                    JSONObject content = o.getJSONObject("content");

                    res = content.has(Locale.getDefault().getLanguage()) ? content.getJSONObject(Locale.getDefault().getLanguage()) : content.getJSONObject(filterLang);
                }
            }
            return res;
        } catch (Exception e) {
            Logger.e(TAG, "EXCEPTION downloading url", e);
        }
        return null;
    }

    private OkHttpClient getOkHttpClient(boolean addInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (addInterceptor)
            builder.addInterceptor(new HttpInterceptor());

//        if (BuildConfig.DEBUG) {
//            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//            builder.addInterceptor(logging);
//        }

        return builder.build();
    }
}
