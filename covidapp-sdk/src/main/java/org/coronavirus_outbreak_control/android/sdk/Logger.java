package org.coronavirus_outbreak_control.android.sdk;

import android.util.Log;

import org.coronavirus_outbreak_control.android.sdk.enums.LoggingMode;

final class Logger {
    public static void d(String tag, String message) {
        if (isSdkFeedbackMode()) {
            Log.d(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (isSdkFeedbackMode()) {
            Log.e(tag, message);
        }
    }

    public static void e(String tag, String message, Throwable exception) {
        if (isSdkFeedbackMode()) {
            Log.e(tag, message, exception);
        }
    }

    private static boolean isSdkFeedbackMode() {
        return CovidApplicationSDK.getInstance() != null &&
                (CovidApplicationSDK.getInstance().loggingMode() == LoggingMode.ALL ||
                        CovidApplicationSDK.getInstance().loggingMode() == LoggingMode.SDK_INFORMATION);
    }
}
